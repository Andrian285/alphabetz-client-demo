import React from 'react'
import { withRouter } from 'react-router-dom'
import SearchIcon from '@material-ui/icons/Search'
import MenuIcon from '@material-ui/icons/Menu'
import { switchSideMenu } from '../actions/main'
import { connect } from 'react-redux'
// import { Button } from '@material-ui/core'
// import { ReactComponent as BadmintonIcon } from '../assets/icons/badminton.svg'
// import { ReactComponent as BaseballIcon } from '../assets/icons/baseball.svg'
// import { ReactComponent as BasketballIcon } from '../assets/icons/basketball.svg'
// import { ReactComponent as PingpongIcon } from '../assets/icons/ping-pong.svg'
// import { ReactComponent as RugbyIcon } from '../assets/icons/rugby.svg'
// import { ReactComponent as RunnerIcon } from '../assets/icons/runner.svg'
// import { ReactComponent as SoccerIcon } from '../assets/icons/soccer.svg'
// import { ReactComponent as SwimmingIcon } from '../assets/icons/swimming.svg'
// import { ReactComponent as VolleyballIcon } from '../assets/icons/volleyball.svg'

const mapDispatchToProps = {
    switchSideMenu
}

export default connect(null, mapDispatchToProps)(withRouter(class extends React.Component {
    render() {
        return (
            <div className='bar'>
                <button className='button-basic' onClick={() => this.props.switchSideMenu()}>
                    <MenuIcon className='icon' />
                </button>
                <div className='title'>
                    Alphabetz
                </div>
                <div className='search'>
                    <SearchIcon />
                    <input placeholder='Search...' className='field' />
                </div>
                <div>
                    {/* <BadmintonIcon className='sport-icon' />
                    <BaseballIcon className='sport-icon' />
                    <BasketballIcon className='sport-icon' />
                    <PingpongIcon className='sport-icon' />
                    <RugbyIcon className='sport-icon' />
                    <RunnerIcon className='sport-icon' />
                    <SoccerIcon className='sport-icon' />
                    <SwimmingIcon className='sport-icon' />
                    <VolleyballIcon className='sport-icon' /> */}
                </div>
                <div className='right'>
                    <button className='button-basic' onClick={() => {
                        this.props.history.push('/login')
                    }}>Login</button>
                    <button className='button-basic' onClick={() => {
                        this.props.history.push('/signup')
                    }}>Sign up</button>
                </div>
            </div>
        )
    }
}))
