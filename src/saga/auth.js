import { put, takeEvery, all, call } from 'redux-saga/effects'
import * as actions from '../actions/auth'
import request from '../utils/request'
import Cookie from 'js-cookie'

export function* login(action) {
    try {
        if (!action.data.email || !action.data.password) yield put(actions.loginFail('Fill all fields please!'))
        else {
            const res = yield call(request, '/api/login', {
                method: 'post',
                body: action.data
            })
            Cookie.set('token', res.data.token)
            yield put(actions.loginSuccess(res.data))
        }
    } catch (e) {
        yield put(actions.loginFail(e.response.data.message))
    }
}

export function* signup(action) {
    try {
        if (!action.data.email || !action.data.password) yield put(actions.loginFail('Fill all fields please!'))
        else if (action.data.password !== action.data.repeat) yield put(actions.signupFail('Password and its confirms are not the same!'))
        else {
            const res = yield call(request, '/api/signup', {
                method: 'post',
                body: action.data
            })
            yield put(actions.signupSuccess(res.data))
        }
    } catch (e) {
        yield put(actions.signupFail(e.response.data.message))
    }
}

export default all([
    takeEvery(actions.LOGIN, login),
    takeEvery(actions.SIGNUP, signup)
])