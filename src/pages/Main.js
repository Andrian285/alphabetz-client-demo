import React from 'react'
import { connect } from 'react-redux'
import Table from '../components/Table'
import SideMenu from '../components/SideMenu'
import Details from '../components/Details'
import Bar from '../components/Bar'

const mapStateToProps = (state) => ({
    sideMenuOpened: state.main.sideMenuOpened
})

export default connect(mapStateToProps)(class extends React.Component {
    render() {
        return (
            <div>
                <SideMenu />
                <Bar />
                <div className='forks-container'>
                    <div className='table-container'><Table /></div>
                    <div className='details-container'><Details /></div>
                </div>
            </div>
        )
    }
})