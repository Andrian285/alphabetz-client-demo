import { defaultActionCreator } from './actionCreator'

export const SWITCH_SIDE_MENU = 'SWITCH_SIDE_MENU'
export const switchSideMenu = defaultActionCreator(SWITCH_SIDE_MENU, 'sideMenuOpened')