import React from 'react'
import SignUp from '../forms/SignUp'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router-dom'
import { signup, resetErrors, resetSignupData } from '../actions/auth'
import ErrorAlert from '../components/ErrorAlert'

const mapStateToProps = (state) => ({
    signupError: state.auth.signupError,
    signupMessage: state.auth.signupMessage
})

const mapDispatchToProps = {
    signup,
    resetErrors,
    resetSignupData
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(class extends React.Component {
    render() {
        if (this.props.signupMessage) {
            this.props.resetSignupData()
            return <Redirect to='/login' />
        }
        return (
            <div className='unauthorized-container'>
                <div className='unauthorized-bar'>
                    <div className='title' onClick={() => this.props.history.push('/')}>Alphabetz</div>
                    <div className='content'>Already have an account?&nbsp;<a href='/login' className='link'>Log in</a></div>
                </div>
                <div className='content'>
                    <div className='form-container'>
                        <ErrorAlert error={this.props.signupError} resetError={() => this.props.resetErrors()} />
                        <SignUp onSubmit={props => this.props.signup(props)} />
                    </div>
                </div>
            </div>
        )
    }
}))