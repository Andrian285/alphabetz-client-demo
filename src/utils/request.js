import axios from 'axios'
import Cookie from 'js-cookie'

export default async function request(link, options) {
    if (options.queries) {
        let query = Object.keys(options.queries).map(key => `${key}=${options.queries[key]}`).join('&')
        let startCharacter = link.includes('?') ? '&' : '?'
        link += startCharacter + query
    }

    let method = options.method
    let response
    let configs = {
        headers: {
            token: Cookie.get('token')
        }
    }

    try {
        if (method.toLowerCase() === 'post') response = await axios.post(link, options.body, configs)
        else if (method.toLowerCase() === 'put') response = await axios.put(link, options.body, configs)
        else response = await axios.get(link, configs)
    } catch (err) {
        if (err.response && err.response.status === 401) {
            Cookie.remove('token')
        }
        throw err
    }
    return response
}