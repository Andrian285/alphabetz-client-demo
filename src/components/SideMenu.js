import React from 'react'
import Checkbox from '@material-ui/core/Checkbox'
import PerfectScrollbar from 'react-perfect-scrollbar'
import { switchSideMenu } from '../actions/main'
import { connect } from 'react-redux'
import 'react-perfect-scrollbar/dist/css/styles.css'
import { isUndefined } from 'util'

const bookies = [
    'Bookie1',
    'Bookie2',
    'Bookie3',
    'Bookie4',
    'Bookie5',
    'Bookie6',
    'Bookie7',
    'Bookie8',
    'Bookie9'
]

const leagues = [
    'League1',
    'League2',
    'League3',
    'League4',
    'League5',
    'League6',
    'League7',
    'League8',
    'League9'
]

const sports = [
    'Basketball',
    'Football',
    'Valleyball',
    'Swimming',
    'Ping-pong',
    'Tennis'
]

const getClassName = (state, basic, animation_open, animation_close) => {
    if (isUndefined(state)) return basic
    return state ? `${basic} ${animation_open}` : `${basic} ${animation_close}`
}

const ListElement = (props) => (
    <div className={props.last ? 'list-element last' : 'list-element'}>
        <div className='list-element-content'>
            <Checkbox
                checked={true}
                inputProps={{
                    'aria-label': 'primary checkbox',
                }}
            />
            {props.data}
        </div>
    </div>
)

const FilterSection = (props) => (
    <>
        <div
            className={props.state ? 'button primary title' : 'button title'}
            onClick={props.onTitleClick}
        >{props.title}</div>
        <div className={getClassName(props.state, 'content', 'show-content', 'hide-content')}>
            <PerfectScrollbar>
                {props.data.map((elm, key) =>
                    <ListElement data={elm} key={key} last={key === props.data.length - 1} />
                )}
            </PerfectScrollbar>
        </div>
    </>
)

//() => this.setState({ showLeagues: !this.state.showLeagues })

const mapDispatchToProps = {
    switchSideMenu
}

const mapStateToProps = (state) => ({
    sideMenuOpened: state.main.sideMenuOpened
})

export default connect(mapStateToProps, mapDispatchToProps)(class extends React.Component {
    state = {
        showBookies: undefined,
        showLeagues: undefined,
        showSports: undefined
    }

    render() {
        return (
            <>
                <div className={getClassName(this.props.sideMenuOpened, 'menu', 'open-menu', 'close-menu')}>
                    <div className='logo'>Alphabetz</div>
                    <div className='actions'>
                        <button className='button'>Live</button>
                        <button className='button'>Prematch</button>
                        <div className='filters'>
                            <div>
                                <FilterSection
                                    title='Bookies'
                                    state={this.state.showBookies}
                                    data={bookies}
                                    onTitleClick={() => this.setState({ showBookies: !this.state.showBookies })}
                                />
                                <FilterSection
                                    title='Leagues'
                                    state={this.state.showLeagues}
                                    data={leagues}
                                    onTitleClick={() => this.setState({ showLeagues: !this.state.showLeagues })}
                                />
                                <FilterSection
                                    title='Sports'
                                    state={this.state.showSports}
                                    data={sports}
                                    onTitleClick={() => this.setState({ showSports: !this.state.showSports })}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    className={getClassName(this.props.sideMenuOpened, 'side-menu-background', 'show-background', 'hide-background')}
                    onClick={() => this.props.switchSideMenu()}
                />
            </>
        )
    }
})