import { defaultActionCreator } from './actionCreator'

export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = `${LOGIN}_SUCCESS`
export const LOGIN_FAIL = `${LOGIN}_FAIL`
export const RESET_LOGIN_DATA = `RESET_${LOGIN}_DATA`

export const login = defaultActionCreator(LOGIN, 'data')
export const loginSuccess = defaultActionCreator(LOGIN_SUCCESS, 'loginMessage')
export const loginFail = defaultActionCreator(LOGIN_FAIL, 'loginError')
export const resetLoginData = defaultActionCreator(RESET_LOGIN_DATA)

export const SIGNUP = 'SIGNUP'
export const SIGNUP_SUCCESS = `${SIGNUP}_SUCCESS`
export const SIGNUP_FAIL = `${SIGNUP}_FAIL`
export const RESET_SIGNUP_DATA = `RESET_${SIGNUP}_DATA`

export const signup = defaultActionCreator(SIGNUP, 'data')
export const signupSuccess = defaultActionCreator(SIGNUP_SUCCESS, 'signupMessage')
export const signupFail = defaultActionCreator(SIGNUP_FAIL, 'signupError')
export const resetSignupData = defaultActionCreator(RESET_SIGNUP_DATA)

export const RESET_ERRORS = 'RESET_ERRORS'
export const resetErrors = defaultActionCreator(RESET_ERRORS)