import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import reducer from './reducers'
import rootSaga from './saga'
import Main from './pages/Main'
import SignUp from './pages/SignUp'
import Login from './pages/Login'
import './styles/index.scss'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(rootSaga)

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path='/' component={Main} />
                        <Route exact path='/signup' component={SignUp} />
                        <Route exact path='/login' component={Login} />
                    </Switch>
                </BrowserRouter>
            </Provider>
        )
    }
}

//<Route render={() => <Error error='Not Found :(' />} />

ReactDOM.render(
    <App />, document.getElementById('root')
)