import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { Person, Lock } from '@material-ui/icons'

let ContactForm = props => {
    const { handleSubmit } = props
    return (
        <div>
            <div className='title'>
                Sign up
            </div>
            <form className='form' onSubmit={handleSubmit}>
                <div className='input-with-icon full-width separated'>
                    <Person className='icon' />
                    <Field component='input' name='email' placeholder='Email' type='email' className='input-field' />
                </div>
                <div className='input-with-icon full-width separated'>
                    <Lock className='icon' />
                    <Field component='input' name='password' placeholder='Password' type='password' className='input-field' />
                </div>
                <div className='input-with-icon full-width separated'>
                    <Lock className='icon' />
                    <Field component='input' name='repeat' placeholder='Repeat password' type='password' className='input-field' />
                </div>
                <div className='submit-container'>
                    <button className='button nice primary' type='submit'>Submit</button>
                </div>
            </form>
        </div>
    )
}

ContactForm = reduxForm({
    form: 'sign_up'
})(ContactForm)

export default ContactForm