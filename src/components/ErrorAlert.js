import React from 'react'
import { Error, Close } from '@material-ui/icons'

export default class extends React.Component {
    closeAlert = null
    state = {
        className: 'alert',
        error: undefined
    }

    showAlert = () => this.setState({ className: 'alert show-alert' })

    hideAlert = () => {
        if (this.closeAlert) clearTimeout(this.closeAlert)
        this.setState({ className: 'alert hide-alert' })
        setTimeout(() => {
            this.setState({ error: undefined })
            this.props.resetError()
        }, 500)
    }

    showAlertTemporarily = () => {
        this.setState({ error: this.props.error })
        this.showAlert()
        this.closeAlert = setTimeout(this.hideAlert, 5000)
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.error && this.props.error) this.showAlertTemporarily()
        else if (prevProps.error && this.props.error && this.props.error !== prevProps.error) {
            if (this.closeAlert) clearTimeout(this.closeAlert)
            this.setState({ className: 'alert hide-alert' })
            setTimeout(this.showAlertTemporarily, 500)
        }
    }

    componentWillUnmount() {
        clearTimeout(this.closeAlert)
    }

    render() {
        return (
            <div className={this.state.className}>
                <Error className='error-icon' />
                <div className='content'>{this.state.error}</div>
                <Close className='close-icon' onClick={this.hideAlert} />
            </div>
        )
    }
}