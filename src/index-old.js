import React from 'react'
import ReactDOM from 'react-dom'
import { Table, TableBody, TableCell, TableRow, TableHead, Button } from '@material-ui/core'
import { ClipLoader } from 'react-spinners'
import axios from 'axios'
import './index.css'

const pages = [
    { title: 'Главная', icon: 'home' },
    { title: 'Выход', icon: 'power_settings_new' }
]

class NavBar extends React.Component {
    render() {
        return (
            <div style={{ position: 'fixed', height: '100%', width: 300, background: '#212120', color: '#eaeaea' }}>
                <div style={{ textAlign: 'center', fontSize: 25, fontFamily: 'Gugi', margin: 15 }}>
                    ALPHABETZ
                </div>
                <hr style={{ background: '#eaeaea', width: '80%' }} />
                <div style={{ marginLeft: 40 }}>
                    {pages.map(page =>
                        <div key={page.title} style={{ marginTop: 40, color: '#dbdbdb', fontSize: 20, display: 'flex' }}>
                            <i className='material-icons'>{page.icon}</i>
                            <div style={{ marginLeft: 10 }}>{page.title}</div>
                        </div>
                    )}
                </div>
                <div style={{ margin: 40 }}>
                    <Button variant='contained' color='secondary' onClick={this.props.onUpdate} disabled={this.props.loading}>Update Data</Button>
                </div>
            </div>
        )
    }
}

class App extends React.Component {
    state = {
        matches: [],
        loading: false
    }

    componentDidMount() {
        this.getMatches()
    }

    updateMatches = async () => {
        this.setState({ loading: true })
        try {
            await axios.get('/update-data')
        } catch (err) {
            window.alert('Smth went wrong :)')
        }
        this.setState({ loading: false })
    }

    getMatches = async () => {
        let res = await axios.get('/get-data')
        this.setState({ matches: res.data })
    }

    getTableHeaderValue = (elm, name) => {
        if (elm.markets) {
            let found = elm.markets[0].runners.find(x => x.name === name)
            if (found) return found.price
        }
    }

    render() {
        return (
            <div>
                <NavBar onUpdate={this.updateMatches} loading={this.state.loading} />
                <div style={{
                    position: 'relative', float: 'right', width: 'calc(100% - 300px)',
                    display: 'flex', flexDirection: 'column', alignItems: 'center', margin: 30
                }}>
                    {this.state.loading ?
                        <ClipLoader
                            sizeUnit={'px'}
                            size={150}
                            color={'#123abc'}
                        /> :
                        <div style={{ width: 700, borderRadius: 10, background: 'white' }}>
                            {this.state.matches.length > 0 &&
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Котора</TableCell>
                                            <TableCell>Имя</TableCell>
                                            <TableCell align='right'>П1</TableCell>
                                            <TableCell align='right'>Х</TableCell>
                                            <TableCell align='right'>П2</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.state.matches.map((elm, key) => (
                                            <TableRow key={key}>
                                                <TableCell component='th' scope='row'>
                                                    {elm.bookie}
                                                </TableCell>
                                                <TableCell align='right'>{elm.match.name}</TableCell>
                                                <TableCell align='right'>{elm.match.p1}</TableCell>
                                                <TableCell align='right'>{elm.match.x}</TableCell>
                                                <TableCell align='right'>{elm.match.p2}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            }
                        </div>
                    }
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <App />, document.getElementById('root')
)