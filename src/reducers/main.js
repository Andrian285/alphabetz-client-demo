import {
    SWITCH_SIDE_MENU
} from '../actions/main';

export default function reducer(state = [], action) {
    switch (action.type) {
        case SWITCH_SIDE_MENU:
            return {
                ...state,
                sideMenuOpened: !state.sideMenuOpened
            }
        default:
            return state
    }
}