import React from 'react'
import footbal_icon from '../assets/icons/soccer.svg'

const get_icon = (data) => {
    return footbal_icon
}

const data = {
    icon: get_icon(),
    name: 'Al-Jahra — Al-Fahaheel',
    league: 'NFS shift',
    coef: 1.2,
    fork: {
        profit: 0.034
    }
}

const data1 = {
    icon: get_icon(),
    name: 'Al-Jahra — Al-Fahaheelsssssss',
    league: 'NFS shift',
    coef: 1.2,
    fork: {
        profit: 0.034
    }
}

class Head extends React.Component {
    render() {
        return (
            <div className='head'>
                <div className='icon-profit-head'>%</div>
                <div className='event-head'>Event</div>
                <div className='bookies-head'>Bookies</div>
                <div className='outcome-head'>Outcome</div>
                <div className='coefficients-head'>Coefficients</div>
                <div className='time-head'>Lifetime</div>
            </div>
        )
    }
}

class Row extends React.Component {
    render() {
        return (
            <div className='row' style={this.props.style}>
                <div className='icon-profit'>
                    <img src={this.props.data.icon} alt='sport' className='icon' />
                    <div className='profit'>{`${(this.props.data.fork.profit * 100).toFixed(2)}%`}</div>
                </div>
                <div className='event'>
                    <div className='name'>{this.props.data.name}</div>
                    <div className='league'>{this.props.data.league}</div>
                </div>
                <div className='bookies'>
                    <div>1xbet</div>
                    <div>Fonbet</div>
                </div>
                <div className='outcome'>
                    <div>P2</div>
                    <div>P2</div>
                </div>
                <div className='coefficients'>
                    <div>1.05</div>
                    <div>1.08</div>
                </div>
                <div className='time'>
                    126s
                </div>
            </div>
        )
    }
}

export default class extends React.Component {
    // style={{ borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}
    render() {
        return (
            <div className='table'>
                <Head />
                {[...Array(20).keys()].map(elm =>
                    <Row data={data} key={elm} />
                )}
            </div>
        )
    }
}