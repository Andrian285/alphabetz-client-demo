import {
    LOGIN,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    RESET_LOGIN_DATA,
    SIGNUP,
    SIGNUP_FAIL,
    SIGNUP_SUCCESS,
    RESET_SIGNUP_DATA,
    RESET_ERRORS
} from '../actions/auth'

export default function reducer(state = [], action) {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                data: action.data
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                loginMessage: action.loginMessage
            }
        case LOGIN_FAIL:
            return {
                ...state,
                loginError: action.loginError
            }
        case RESET_LOGIN_DATA:
            return {
                ...state,
                data: undefined,
                loginMessage: undefined,
                loginError: undefined
            }
        case SIGNUP:
            return {
                ...state,
                data: action.data
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                signupMessage: action.signupMessage
            }
        case SIGNUP_FAIL:
            return {
                ...state,
                signupError: action.signupError
            }
        case RESET_SIGNUP_DATA:
            return {
                ...state,
                data: undefined,
                signupMessage: undefined,
                signupError: undefined
            }
        case RESET_ERRORS:
            return {
                ...state,
                loginError: undefined,
                signupError: undefined
            }
        default:
            return state
    }
}