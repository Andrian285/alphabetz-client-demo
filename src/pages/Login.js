import React from 'react'
import { withRouter, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import Login from '../forms/Login'
import ErrorAlert from '../components/ErrorAlert'
import { login, resetErrors, resetLoginData } from '../actions/auth'

const mapStateToProps = (state) => ({
    loginError: state.auth.loginError,
    loginMessage: state.auth.loginMessage
})

const mapDispatchToProps = {
    login,
    resetErrors,
    resetLoginData
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(class extends React.Component {
    render() {
        if (this.props.loginMessage) {
            this.props.resetLoginData()
            return <Redirect to='/' />
        }
        return (
            <div className='unauthorized-container'>
                <div className='unauthorized-bar'>
                    <div className='title' onClick={() => this.props.history.push('/')}>Alphabetz</div>
                    <div>&nbsp;</div>
                </div>
                <div className='content'>
                    <div className='form-container'>
                        <ErrorAlert error={this.props.loginError} resetError={() => this.props.resetErrors()} />
                        <Login onSubmit={props => this.props.login(props)} />
                        <div className='login-link'>
                            <div className='info'>
                                New to Alphabetz?
                            </div>
                            <button className='button nice' onClick={() => this.props.history.push('/signup')}>Sign up</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}))
